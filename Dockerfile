FROM node:15-alpine

WORKDIR /app
COPY . . 
RUN npm install 
RUN npm install -g pm2
CMD ["pm2-runtime", "ecosystem.config.js", "--env", "production"]
